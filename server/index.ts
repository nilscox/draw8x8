/// <reference path="typings/index.d.ts" />

import * as express from 'express';
import * as bodyParser from 'body-parser';
import * as session from 'express-session';

import {Application} from './app/Application';

import {UserController} from './api/controllers/UserController';
import {WordController} from './api/controllers/WordController';
import {GameController} from './api/controllers/GameController';
import {AnswerController} from './api/controllers/AnswerController';

let app: Application = new Application();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(session({
    secret: 'FIXME',
    cookie: { maxAge: null },
    resave: false,
    saveUninitialized: false
}));

let userCtrl = new UserController();
let wordCtrl = new WordController();
let gameCtrl = new GameController();
let answerCtrl = new AnswerController();

gameCtrl.include('authorId', 'author', userCtrl);
gameCtrl.include('wordId', 'word', wordCtrl);

answerCtrl.include('wordId', 'word', wordCtrl);
answerCtrl.include('gameId', 'game', gameCtrl);
answerCtrl.include('userId', 'user', userCtrl);

app.addController('/user', userCtrl);
app.addController('/word', wordCtrl);
app.addController('/game', gameCtrl);
app.addController('/answer', answerCtrl);

app.use((req: express.Request, res: express.Response) => {
    res.status(404).end();
});

app.listen(4242);
