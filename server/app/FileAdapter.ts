import * as fs from 'fs';
import * as path from 'path';
import {ObjectAdapter} from './ObjectAdapter';

export abstract class FileAdapter<T> extends ObjectAdapter<T> {
    private path: string;
    private extension: string;

    constructor(path: string, extension: string) {
        super();
        this.path = path;
        this.extension = extension;
    }

    protected abstract serialize(obj: T): string;
    protected abstract deserialize(data: string): T;

    public all(predicate): T[] {
        let results: T[] = [];

        fs.readdirSync(this.path).forEach((filename: string) => {
            let data = null;

            try {
                data = fs.readFileSync(path.join(this.path, filename), { encoding: 'UTF-8' });
            } catch (err) {
                if ('code' in err && err.code === 'ENOENT')
                    return null;
            }

            let t: T = this.deserialize(data);
            if (predicate(t))
                results.push(t);
        });

        return results;
    }

    public fetch(id: any): T {
        let data = null;

        try {
            data = fs.readFileSync(path.join(this.path, id) + '.' + this.extension, { encoding: 'UTF-8' });
        } catch (err) {
            if ('code' in err && err.code === 'ENOENT')
                return null;
        }

        return this.deserialize(data);
    }

    private static randomId(): string {
        let result: string = '';

        for (let j = 0; j < 16; j++)
            result += Math.floor(Math.random() * 16).toString(16).toUpperCase();

        return result;
    }

    public create(obj: T): Promise<T> {
        let id = FileAdapter.randomId();
        obj['id'] = id;

        return new Promise((resolve, reject) => {
            fs.writeFile(
                path.join(this.path, id) + '.' + this.extension,
                this.serialize(obj),
                err => {
                    if (err) reject(err);
                    else resolve(obj);
                });
        });
    }

    public read(id: string): Promise<T> {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(this.path, id) + '.' + this.extension, (err, data) => {
                if (err) {
                    if ('code' in err && err.code === 'ENOENT') resolve(null);
                    else reject(err);
                } else resolve(this.deserialize(data.toString()));
            });
        });
    }

    public update(id: string, obj: T): Promise<T> {
        if ('id' in obj)
            delete obj['id'];

        return new Promise((resolve, reject) => {
            fs.readFile(path.join(this.path, id) + '.' + this.extension, (err, data) => {
                if (err) {
                    if ('code' in err && err.code === 'ENOENT') resolve(null);
                    else reject(err);
                } else {
                    let updated: T = this.deserialize(data.toString());

                    Object.assign(updated, obj);

                    fs.writeFile(
                        path.join(this.path, id) + '.' + this.extension,
                        this.serialize(updated),
                        err => {
                            if (err) reject(err);
                            else resolve(updated);
                        });
                }
            });
        });
    }

    public remove(id: string): Promise<T> {
        return new Promise((resolve, reject) => {
            fs.readFile(path.join(this.path, id) + '.' + this.extension, (err, data) => {
                if (err) {
                    if ('code' in err && err.code === 'ENOENT') resolve(null);
                    else reject(err);
                } else {
                    fs.unlink(path.join(this.path, id) + '.' + this.extension, err => {
                        if (err) reject(err);
                        else resolve(this.deserialize(data.toString()));
                    });
                }
            });
        });
    }
}