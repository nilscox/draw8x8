import * as express from 'express';

interface Route {
    path: string;
    method: string;
    handler: express.RequestHandler;
}

export class Controller {
    private router: express.Router = null;
    private routes: Route[] = [];

    public configure(router: express.Router) {
        this.router = router;

        this.router.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            if (req.body)
                req.body = this.validate(req.body);
            next();
        });

        for (let route of this.routes.reverse())
            this.router[route.method](route.path, route.handler.bind(this));
    }

    private addRoute(path: string, method: string, handler: express.RequestHandler) {
        this.routes.push({
            path: path,
            method: method,
            handler: handler
        });
    }

    protected validate(body: any): any {
        return body;
    }

    public get(path: string, handler: express.RequestHandler) {
        this.addRoute(path, 'get', handler);
    }

    public post(path: string, handler: express.RequestHandler) {
        this.addRoute(path, 'post', handler);
    }

    public put(path: string, handler: express.RequestHandler) {
        this.addRoute(path, 'put', handler);
    }

    public delete(path: string, handler: express.RequestHandler) {
        this.addRoute(path, 'delete', handler);
    }
}