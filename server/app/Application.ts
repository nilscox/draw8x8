/// <reference path="../typings/index.d.ts"/>

import * as express from 'express';
import {Controller} from './Controller';

export class Application {
    private app = express();

    public addController(path: string, ctrl: Controller) {
        let router = express.Router();
        ctrl.configure(router);
        this.app.use(path, router);
    }

    public use(middleware: express.RequestHandler) {
        this.app.use(middleware);
    }

    public listen(port: number) {
        this.app.listen(port);
    }
}