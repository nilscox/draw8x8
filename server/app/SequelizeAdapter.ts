import * as Sequelize from 'sequelize';
import {ObjectAdapter} from './ObjectAdapter';

interface TAttributes {}

interface TInstance<TInst, TAttrs extends TAttributes> {
    update(TAttrs, options?: Sequelize.InstanceUpdateOptions): Promise<TInst>;
    destroy(options?: Sequelize.InstanceDestroyOptions): Promise<void>;
}

export class SequelizeAdapter<TInst extends TInstance<TInst, TAttrs>, TAttrs extends TAttributes> extends ObjectAdapter<TAttrs> {
    private model: Sequelize.Model<TInst, TAttrs> = null;

    private createOptions: Sequelize.CreateOptions = null;
    private findOptions: Sequelize.FindOptions = null;
    private updateOptions: Sequelize.UpdateOptions = null;
    private destroyOptions: Sequelize.DestroyOptions = null;

    constructor(model: Sequelize.Model<TInst, TAttrs>) {
        super();
        this.model = model;
    }

    public setCreateOptions(options: Sequelize.CreateOptions) {
        this.createOptions = options;
    }

    public setFindOptions(options: Sequelize.FindOptions) {
        this.findOptions = options;
    }

    public setUpdateOptions(options: Sequelize.UpdateOptions) {
        this.updateOptions = options;
    }

    public setDestroyOptions(options: Sequelize.DestroyOptions) {
        this.destroyOptions = options;
    }

    public getModel(): Sequelize.Model<TInst, TAttrs> {
        return this.model;
    }

    public all(predicate): TAttrs[] {
        return null;
    }

    fetch(id: number): TAttrs {
        return null;
    }

    public create(obj: TAttrs): Promise<TAttrs> {
        return new Promise((resolve, reject) => {
            this.model.create(obj, this.createOptions).then((inst: TInst) => {
                if (null == inst) resolve(null);
                else this.model.findById(inst['id'], this.findOptions).then(resolve);
            }).catch(reject);
        });
    }

    public read(id: number): Promise<TAttrs> {
        return new Promise((resolve, reject) => {
            this.model.findById(id, this.findOptions).then(resolve).catch(reject);
        });
    }

    public update(id: number, obj: TAttrs): Promise<TAttrs> {
        return new Promise((resolve, reject) => {
            this.model.findById(id).then((inst: TInst) => {
                if (null == inst) resolve(null);
                else inst.update(obj, this.updateOptions).then((inst: TInst) => {
                    if (null == inst) reject();
                    else this.model.findById(inst['id'], this.findOptions).then((inst: TInst) => {
                        if (null == inst) resolve(null);
                        else resolve(inst);
                    });
                });
            }).catch(reject);
        });
    }

    public remove(id: number): Promise<TAttrs> {
        return new Promise((resolve, reject) => {
            this.model.findById(id, this.findOptions).then((inst: TInst) => {
                if (null == inst) resolve(null);
                else inst.destroy(this.destroyOptions).then(() => resolve(inst));
            }).catch(reject);
        });
   }
}