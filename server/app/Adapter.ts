export interface Adapter<T extends { id?: string | number }> {
    all(predicate): T[];
    fetch(id: any): T;
    create(obj: T): Promise<T>;
    read(id: any): Promise<T>;
    update(id: any, obj: T): Promise<T>;
    remove(id: any): Promise<T>;
}