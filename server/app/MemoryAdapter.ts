import {ObjectAdapter} from './ObjectAdapter';

export class MemoryAdapter<T> extends ObjectAdapter<T> {
    private values: T[] = [];

    all(predicate): T[] {
        return this.values.filter(predicate);
    }

    fetch(id: number): T {
        if (!(id in this.values))
            return null;
        return this.values[id];
    }

    create(obj: T): Promise<T> {
        return new Promise(resolve => {
            obj['id'] = this.values.length + 1;
            this.values.push(obj);
            resolve(this.values[this.values.length - 1]);
        });
    }

    read(id: number): Promise<T> {
        id--;
        return new Promise(resolve => {
            if (!(id in this.values))
                resolve(null);
            else
                resolve(this.values[id]);
        });
    }

    update(id: number, obj: T): Promise<T> {
        id--;
        return new Promise(resolve => {
            if (!(id in this.values))
                resolve(null);
            else {
                obj['id'] = id + 1;
                this.values[id] = obj;
                resolve(this.values[id]);
            }
        });
    }

    remove(id: number): Promise<T> {
        id--;
        return new Promise(resolve => {
            if (!(id in this.values))
                resolve(null);
            else {
                let obj: T = this.values[id];
                this.values[id] = null;
                resolve(obj);
            }
        });
    }

}