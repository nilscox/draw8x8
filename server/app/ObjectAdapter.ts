import {Adapter} from './Adapter';
import {ObjectController} from './ObjectController';

export abstract class ObjectAdapter<T> implements Adapter<T> {
    private includes: { [fkey: string]: { jsonKey: string, controller: ObjectController<any> } } = {};

    public include<U>(fkey: string, jsonKey: string, ctrl: ObjectController<U>) {
        this.includes[fkey] = {
            jsonKey: jsonKey,
            controller: ctrl
        };
    }

    public getIncludes(inst: T | T[]) {
        if (inst instanceof Array)
            return inst.map(this.getIncludes.bind(this));

        let includes = {};

        for (let fkey in this.includes) {
            if (null == inst[fkey])
                continue;

            let ctrl = this.includes[fkey].controller;
            let include = ctrl.adapter.fetch(inst[fkey]);
            let subincludes = ctrl.getIncludes(include);
            for (let jsonKey in subincludes)
                include[jsonKey] = subincludes[jsonKey];
            includes[this.includes[fkey].jsonKey] = include;
        }

        return includes;
    }

    public abstract all(predicate): T[];
    public abstract fetch(id: any): T;
    public abstract create(obj: T): Promise<T>;
    public abstract read(id: any): Promise<T>;
    public abstract update(id: any, obj: T): Promise<T>;
    public abstract remove(id: any): Promise<T>;
}