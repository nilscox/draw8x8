import {FileAdapter} from './FileAdapter';

export class JSONAdapter<T> extends FileAdapter<T> {
    constructor(filename: string) {
        super(filename, 'json');
    }

    protected serialize(obj: T): string {
        return JSON.stringify(obj);
    }

    protected deserialize(data: string): T {
        return JSON.parse(data);
    }
}