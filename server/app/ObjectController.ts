import * as express from 'express';
import {Controller} from './Controller';
import {ObjectAdapter} from './ObjectAdapter';

export class ObjectController<T extends { id?: number }> extends Controller {
    public adapter: ObjectAdapter<T> = null;

    constructor(adapter: ObjectAdapter<T>) {
        super();

        this.adapter = adapter;

        this.post('/', this.create);
        this.get('/:id', this.read);
        this.put('/:id', this.update);
        this.delete('/:id', this.remove);
    }

    protected validate(body: any): T {
        return body;
    }

    public include<U>(fkey: string, jsonKey: string, ctrl: ObjectController<U>) {
        this.adapter.include(fkey, jsonKey, ctrl)
    }

    public getIncludes(inst: T | T[]) {
        return this.adapter.getIncludes(inst);
    }

    // TODO: handle T[]
    protected onThen(req: express.Request, res: express.Response, inst: T | T[]) {
        if (null === inst)
            return res.status(404).end();

        if (inst instanceof Array)
            inst.map(x => Object.assign(x, this.getIncludes(x)));
        else
            Object.assign(inst, this.getIncludes(inst));

        res.json(inst);
    }

    protected onCatch(req: express.Request, res: express.Response, err: any) {
        res.status(500).json(err);
    }

    private handleResult(req, res: express.Response, promise: Promise<T>) {
        promise
            .then(inst => this.onThen(req, res, inst))
            .catch(err => this.onCatch(req, res, err));
    }

    public create(req: express.Request, res: express.Response) {
        this.handleResult(req, res, this.adapter.create(req.body));
    }

    public read(req: express.Request, res: express.Response) {
        this.handleResult(req, res, this.adapter.read(req.params.id));
    }

    public update(req: express.Request, res: express.Response) {
        this.handleResult(req, res, this.adapter.update(req.params.id, req.body));
    }

    public remove(req: express.Request, res: express.Response) {
        this.handleResult(req, res, this.adapter.remove(req.params.id));
    }
}