import {User} from './user';

export interface Game {
    id?: number;
    width?: number;
    height?: number;
    game?: boolean[][];
    authorId?: number;
    author: User;
}
