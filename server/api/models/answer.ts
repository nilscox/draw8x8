import {Game} from './game';
import {Word} from './word';
import {User} from './user';

export interface Answer {
    id?: number;
    userId?: number;
    gameId?: number;
    wordId?: number;
    game: Game;
    word: Word;
    user: User;
}
