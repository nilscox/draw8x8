import * as express from 'express';
import {ObjectController} from '../../app/ObjectController';
import {JSONAdapter} from '../../app/JSONAdapter';
import {User} from '../models/user';

export class UserController extends ObjectController<User> {
    constructor() {
        super(new JSONAdapter<User>('db/users'));

        this.post('/login', this.login);
        this.get('/logout', this.logout);
        this.get('/current', this.current);
    }

    public login(req: express.Request, res: express.Response) {
        if (!req.body.username)
            return res.status(400).end();

        let users: User[] = this.adapter.all(x => x.username == req.body.username);

        if (!users.length)
            res.status(404).end();
        else {
            req.session['user'] = users[0];
            this.onThen(req, res, users[0]);
        }
    }

    public logout(req: express.Request, res: express.Response) {
        delete req.session['user'];
        res.end();
    }

    public current(req: express.Request, res: express.Response) {
        if (!req.session['user'])
            return res.status(404).end();
        this.onThen(req, res, req.session['user']);
    }
}
