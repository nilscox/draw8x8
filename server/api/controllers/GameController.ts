import * as express from 'express';
import {ObjectController} from '../../app/ObjectController';
import {Game} from '../models/game';
import {JSONAdapter} from '../../app/JSONAdapter';

export class GameController extends ObjectController<Game> {
    constructor() {
        super(new JSONAdapter<Game>('db/games'));
        this.get('/random', this.random);
    }

    protected validate(body: any): Game {
        if ('game' in body && typeof(body.game) == 'string')
            body.game = JSON.parse(body.game);
        return body;
    }

    public random(req: express.Request, res: express.Response) {
        let games: Game[] = this.adapter.all(x => true);
        if (!games.length)
            res.status(404).end();
        else
            this.onThen(req, res, games[parseInt((Math.random() * games.length).toString())]);
    }
}