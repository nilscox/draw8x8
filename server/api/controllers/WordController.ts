import * as express from 'express';
import {ObjectController} from '../../app/ObjectController';
import {Word} from '../models/word';
import {JSONAdapter} from '../../app/JSONAdapter';

export class WordController extends ObjectController<Word> {
    constructor() {
        super(new JSONAdapter<Word>('db/words'));
        this.post('/', this.create);
        this.get('/random', this.random);
    }

    public create(req: express.Request, res: express.Response) {
        let words: Word[] = this.adapter.all(x => x.word == req.body.word);

        if (words.length == 1)
            res.json(words[0]);
        else
            super.create(req, res);
    }

    public random(req: express.Request, res: express.Response) {
        let words: Word[] = this.adapter.all(x => true);
        if (!words.length)
            res.status(404).end();
        else
            res.json(words[parseInt((Math.random() * words.length).toString())]);
    }
}