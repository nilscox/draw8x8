import * as express from 'express';
import {ObjectController} from '../../app/ObjectController';
import {Answer} from '../models/answer';
import {JSONAdapter} from '../../app/JSONAdapter';

export class AnswerController extends ObjectController<Answer> {
    constructor() {
        super(new JSONAdapter<Answer>('db/answers'));
        this.get('/user/:id', this.fromUser);
    }

    public fromUser(req: express.Request, res: express.Response) {
        let answers: Answer[] = this.adapter.all(x => x.userId == req.params.id);
        if (!answers.length)
            res.status(404).end();
        else
            this.onThen(req, res, answers);
    }
}