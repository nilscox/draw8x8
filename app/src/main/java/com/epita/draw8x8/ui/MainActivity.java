package com.epita.draw8x8.ui;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.epita.draw8x8.R;
import com.epita.draw8x8.game.api.GameAPI;
import com.epita.draw8x8.game.models.User;
import com.epita.draw8x8.utils.Consumer;
import com.epita.draw8x8.utils.NetworkUtils;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_USER = "com.epita.draw8x8.EXTRA_USER";

    private User _user;
    private EditText _loginEditText;
    private Button _loginButton;
    private Button _drawButton;
    private Button _guessButton;
    private Button _viewGamesButton;

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putSerializable(EXTRA_USER, _user);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        GameAPI.setIp(getString(R.string.ip));
        GameAPI.setPort(getString(R.string.port));

        _loginEditText = (EditText) findViewById(R.id.login_input);
        _loginButton = (Button) findViewById(R.id.button_login);
        _drawButton = (Button) findViewById(R.id.button_draw);
        _guessButton = (Button) findViewById(R.id.button_guess);
        _viewGamesButton = (Button) findViewById(R.id.view_games_button);

        _loginEditText.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    onClickLogin(v);
                    return true;
                }
                return false;
            }
        });

        if (null != savedInstanceState)
            setUser((User) savedInstanceState.getSerializable(EXTRA_USER));

        if (!NetworkUtils.isConnected(this))
            showConnectivityDialog();
        else {
            NetworkUtils.isServerUp(new Consumer<Boolean>() {
                @Override
                public void accept(Boolean value) {
                    if (!value)
                        showServerDownDialog();
                }
            });
        }
    }

    private void showConnectivityDialog() {
        showDialog("Not connected", "Please check your connectivity status.", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                if (!NetworkUtils.isConnected(MainActivity.this))
                    showConnectivityDialog();
                else {
                    NetworkUtils.isServerUp(new Consumer<Boolean>() {
                        @Override
                        public void accept(Boolean value) {
                            if (!value)
                                showServerDownDialog();
                        }
                    });
                }
            }
        }).show();
    }

    private void showServerDownDialog() {
        showDialog("Server down", "The server is down... :(", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                NetworkUtils.isServerUp(new Consumer<Boolean>() {
                    @Override
                    public void accept(Boolean value) {
                        if (!value)
                            showServerDownDialog();
                    }
                });
            }
        }).show();
    }

    private AlertDialog showDialog(String title, String message, DialogInterface.OnClickListener listener) {
        return new AlertDialog.Builder(this)
            .setTitle(title)
            .setMessage(message)
            .setPositiveButton(R.string.ok, listener)
            .setCancelable(false)
            .create();
    }

    private void setUser(User user) {
        _user = user;
        boolean loggedIn = null != user;

        if (loggedIn) {
            _loginButton.setText(getString(R.string.log_out));
            _loginEditText.setText(_user.getUsername());
        }
        else {
            _loginButton.setText(getString(R.string.log_in));
            _loginEditText.setText("");
        }

        _loginEditText.setEnabled(!loggedIn);
        _drawButton.setEnabled(loggedIn);
        _guessButton.setEnabled(loggedIn);
        _viewGamesButton.setEnabled(loggedIn);
    }

    public void onClickDraw(View view) {
        if (null == _user)
            throw new IllegalStateException();

        Intent intent = new Intent(this, DrawActivity.class);
        intent.putExtra(EXTRA_USER, _user);
        startActivity(intent);
    }

    public void onClickGuess(View view) {
        if (null == _user)
            throw new IllegalStateException();

        Intent intent = new Intent(this, GuessActivity.class);
        intent.putExtra(EXTRA_USER, _user);
        startActivity(intent);
    }

    public void onClickLogin(View view) {
        if (null != _user) {
            GameAPI.user.logout(new Consumer<User>() {
                @Override
                public void accept(User user) {
                    Toast.makeText(MainActivity.this, getString(R.string.logged_out), Toast.LENGTH_SHORT).show();
                    setUser(null);
                }
            });
        }
        else {
            User user = new User(_loginEditText.getText().toString().trim());
            GameAPI.user.login(user, new Consumer<User>() {
                @Override
                public void accept(User user) {
                    if (null == user)
                        Toast.makeText(MainActivity.this, getString(R.string.invalid_username), Toast.LENGTH_SHORT).show();

                    setUser(user);
                }
            });
        }
    }

    public void onClickViewGames(View view) {
        Intent intent = new Intent(this, GamesListActivity.class);
        intent.putExtra(EXTRA_USER, _user);
        startActivity(intent);
    }
}
