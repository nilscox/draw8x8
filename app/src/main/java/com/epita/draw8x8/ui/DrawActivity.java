package com.epita.draw8x8.ui;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.epita.draw8x8.R;
import com.epita.draw8x8.game.api.GameAPI;
import com.epita.draw8x8.game.models.Game;
import com.epita.draw8x8.game.models.User;
import com.epita.draw8x8.game.models.Word;
import com.epita.draw8x8.game.ui.GameView;
import com.epita.draw8x8.utils.Consumer;

public class DrawActivity extends AppCompatActivity {
    private int _gameSize;
    private GameView _gameView;
    private User _user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_draw);

        Intent intent = getIntent();
        _user = (User) intent.getSerializableExtra(MainActivity.EXTRA_USER);

        _gameSize = Integer.parseInt(getString(R.string.game_default_size));
        _gameView = (GameView) findViewById(R.id.draw_view_game);

        fetchWord();
    }

    private void fetchWord() {
        final TextView wordTextView = (TextView) findViewById(R.id.draw_word_textView);
        GameAPI.word.getRandom(new Consumer<Word>() {
            @Override
            public void accept(Word word) {
                if (null == word) {
                    String text = getString(R.string.cannot_find_any_word);
                    Toast.makeText(DrawActivity.this, text, Toast.LENGTH_SHORT).show();
                    finish();
                } else if (null != wordTextView) {
                    _gameView.setGame(new Game(_user, word, _gameSize, _gameSize));
                    wordTextView.setText(word.getWord());
                    wordTextView.postInvalidate();
                }
            }
        });
    }

    public void onFinish(View view) {
        GameAPI.game.add(_gameView.getGame(), new Consumer<Game>() {
            @Override
            public void accept(Game game) {
                if (null == game) {
                    String text = getString(R.string.error_while_adding_game);
                    Toast.makeText(DrawActivity.this, text, Toast.LENGTH_SHORT).show();
                }
                else {
                    String text = getString(R.string.game_added_successfully);
                    Toast.makeText(DrawActivity.this, text, Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}
