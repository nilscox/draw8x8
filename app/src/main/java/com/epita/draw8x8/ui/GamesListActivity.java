package com.epita.draw8x8.ui;


import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.epita.draw8x8.R;
import com.epita.draw8x8.game.api.GameAPI;
import com.epita.draw8x8.game.models.Answer;
import com.epita.draw8x8.game.models.Game;
import com.epita.draw8x8.game.models.User;
import com.epita.draw8x8.game.ui.GameView;
import com.epita.draw8x8.utils.Consumer;

import java.util.List;

public class GamesListActivity extends AppCompatActivity {

    private class AnswerListAdapter extends ArrayAdapter<Answer> {
        AnswerListAdapter(List<Answer> values) {
            super(GamesListActivity.this, R.layout.gamelist_item, values);
        }

        @NonNull
        @Override
        public View getView(int position, View convertView, @NonNull ViewGroup parent) {
            if (null == convertView) {
                LayoutInflater inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                convertView = inflater.inflate(R.layout.gamelist_item, null);
            }

            Answer answer = getItem(position);

            if (null == answer)
                return convertView;

            Game game = answer.getGame();

            TextView correctAnswerTextView = (TextView) convertView.findViewById(R.id.games_list_correct_answer);
            TextView userAnswerTextView = (TextView) convertView.findViewById(R.id.games_list_user_answer);

            correctAnswerTextView.setText(game.getWord().toString());

            if (game.isAnswerCorrect(answer)) {
                correctAnswerTextView.setTextColor(Color.parseColor("#339933"));
                userAnswerTextView.setVisibility(View.GONE);
            } else {
                correctAnswerTextView.setTextColor(Color.parseColor("#993333"));
                userAnswerTextView.setText(String.format(getString(R.string.your_answer), answer.getWord().toString()));
                userAnswerTextView.setVisibility(View.VISIBLE);
            }

            GameView gameView = (GameView) convertView.findViewById(R.id.games_list_gameview);
            gameView.setGame(game);
            gameView.drawGrid(false);

            return convertView;
        }
    }

    private User _user;
    private ListView _listView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gamelist);

        Intent intent = getIntent();
        _user = (User) intent.getSerializableExtra(MainActivity.EXTRA_USER);

        _listView = (ListView) findViewById(R.id.games_list);
        assert _listView != null;

        fetchAnswers();
    }

    private void fetchAnswers() {
        GameAPI.answer.getFromUser(_user.getId(), new Consumer<List<Answer>>() {
            @Override
            public void accept(List<Answer> answers) {
                if (null != answers)
                    _listView.setAdapter(new AnswerListAdapter(answers));
                else {
                    Toast.makeText(GamesListActivity.this, R.string.cannot_find_games, Toast.LENGTH_SHORT).show();
                    finish();
                }
            }
        });
    }
}