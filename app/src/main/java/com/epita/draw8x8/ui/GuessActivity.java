package com.epita.draw8x8.ui;

import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.Html;
import android.view.KeyEvent;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.epita.draw8x8.R;
import com.epita.draw8x8.game.api.GameAPI;
import com.epita.draw8x8.game.models.Answer;
import com.epita.draw8x8.game.models.Game;
import com.epita.draw8x8.game.models.User;
import com.epita.draw8x8.game.models.Word;
import com.epita.draw8x8.game.ui.GameView;
import com.epita.draw8x8.utils.Consumer;

public class GuessActivity extends AppCompatActivity {
    private EditText _input;

    private GameView _gameView;
    private User _user;
    private TextView _authorTextView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_guess);

        Intent intent = getIntent();
        _user = (User) intent.getSerializableExtra(MainActivity.EXTRA_USER);

        _input = (EditText) findViewById(R.id.guess_input);
        _authorTextView = (TextView) findViewById(R.id.guess_author);

        _input.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (EditorInfo.IME_ACTION_DONE == actionId) {
                    onSendClicked(v);
                    return true;
                }
                return false;
            }
        });

        _gameView = (GameView) findViewById(R.id.guess_view_game);
        _gameView.setReadOnly(true);
        newGame();
    }

    private void newGame() {
        _input.setText("");
        _authorTextView.setText("");
        _gameView.setGame(null);

        GameAPI.game.getRandom(new Consumer<Game>() {
            @Override
            public void accept(Game game) {
                if (null == game) {
                    Toast.makeText(GuessActivity.this, R.string.cannot_find_any_game, Toast.LENGTH_SHORT).show();
                    finish();
                }
                else
                {
                    _authorTextView.setText(getString(R.string.guess_author, game.getUser()));
                    _gameView.setGame(game);
                }
            }
        });

    }

    public void onSendClicked(View view) {
        Word word = new Word(_input.getText().toString().trim());
        Answer answer = new Answer(_user, _gameView.getGame(), word);

        GameAPI.answer.add(answer, new Consumer<Answer>() {
            @Override
            public void accept(Answer answer) {
                if (null != answer)
                    createDialog(answer).show();
                else
                    Toast.makeText(GuessActivity.this, R.string.internal_error, Toast.LENGTH_SHORT).show();
                }
        });
    }

    private Dialog createDialog(Answer answer) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);

        if (this._gameView.getGame().isAnswerCorrect(answer))
            builder
                    .setMessage(Html.fromHtml(getString(R.string.message_guess_sucess, answer.getWord())))
                    .setTitle(R.string.correct_answer);
        else
            builder
                    .setMessage(Html.fromHtml(getString(R.string.message_guess_failure, _gameView.getGame().getWord())))
                    .setTitle(R.string.incorrect_answer);

        builder.setPositiveButton(R.string.next, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                newGame();
                dialog.dismiss();
            }
        });

        builder.setNegativeButton(R.string.close, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int id) {
                finish();
            }
        });

        return builder.create();
    }
}
