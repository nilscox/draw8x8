package com.epita.draw8x8.game.models;

import com.epita.draw8x8.game.api.GameAPI;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Answer extends Model {
    public static final AnswerFactory FACTORY = new AnswerFactory();

    private User _user;
    private Game _game;
    private Word _word;

    public Answer(User user, Game game, Word word) {
        _user = user;
        _game = game;
        _word = word;
    }

    private static class AnswerFactory implements ModelFactory<Answer> {
        @Override
        public Answer fromJSON(JSONObject json) throws JSONException {
            User user = User.FACTORY.fromJSON(json.getJSONObject("user"));
            Game game = Game.FACTORY.fromJSON(json.getJSONObject("game"));
            Word word = Word.FACTORY.fromJSON(json.getJSONObject("word"));

            Answer answer = new Answer(user, game, word);
            answer._id = json.getString("id");
            answer.setChanged(false);
            return answer;
        }
    }

    public Word getWord() {
        return _word;
    }

    public Game getGame() {
        return _game;
    }

    @Override
    protected Map<String, Object> getFields() {
        Map<String, Object> map = new HashMap<>();

        map.put("userId", _user.getId());
        map.put("gameId", _game.getId());

        if (null == _word.getId())
            _word = GameAPI.word.save(_word);

        map.put("wordId", _word.getId());

        return map;
    }
}
