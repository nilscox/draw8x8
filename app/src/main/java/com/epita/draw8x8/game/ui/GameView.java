package com.epita.draw8x8.game.ui;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Point;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;

import com.epita.draw8x8.game.models.Game;

import java.util.ArrayList;
import java.util.List;

public class GameView extends View implements View.OnTouchListener {
    private float _viewSize = 0;
    private boolean _readOnly = false;

    private Game _game;
    private GameDrawable _gameDrawable;

    private List<Point> _pointsList = new ArrayList<>();
    private boolean _set = true;

    public GameView(Context context) {
        super(context);
        init();
    }

    public GameView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public GameView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setOnTouchListener(this);
    }

    public void setReadOnly(boolean readOnly) {
        _readOnly = readOnly;
    }

    public Game getGame() {
        return _game;
    }

    public void setGame(Game game) {
        if (null != game) {
            _game = game;
            _gameDrawable = new GameDrawable(_game, _viewSize);
            postInvalidate();
        }
    }

    @Override
    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        // TODO: trouver un moyen de pas bouffer le bouton en mode landscape
        _viewSize =  Math.min(MeasureSpec.getSize(widthMeasureSpec), MeasureSpec.getSize(heightMeasureSpec));
        if (null != _gameDrawable)
            _gameDrawable.setViewSize(_viewSize);
        setMeasuredDimension((int) _viewSize, (int) _viewSize);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        if (null != _game)
            _gameDrawable.draw(canvas);
    }

    @Override
    public boolean onTouch(View view, MotionEvent event) {
        if (_readOnly || null == _game)
            return false;

        Point current = new Point();
        current.x = (int) (_game.getWidth() * event.getX() / _viewSize);
        current.y = (int) (_game.getHeight() * event.getY() / _viewSize);

        if (event.getAction() == MotionEvent.ACTION_DOWN)
            _set = !_game.getCell(current);

        if (event.getAction() == MotionEvent.ACTION_UP)
            _pointsList.clear();

        if (_pointsList.contains(current))
            return false;

        _game.setCell(current, _set);
        _pointsList.add(current);

        invalidate();
        return true;
    }

    public void drawGrid(boolean drawGrid) {
        if (null != _gameDrawable)
            _gameDrawable.drawGrid(drawGrid);
    }
}
