package com.epita.draw8x8.game.ui;

import android.graphics.Canvas;
import android.graphics.ColorFilter;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.graphics.Point;
import android.graphics.drawable.Drawable;

import com.epita.draw8x8.game.models.Game;

public class GameDrawable extends Drawable {
    private float _viewSize;
    private boolean _drawGrid;

    private Paint _pixelPaint;
    private Paint _gridPaint;

    private Game _game;

    public GameDrawable(Game game, float viewSize) {
        _game = game;
        _viewSize = viewSize;
        _drawGrid = true;

        _pixelPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        _pixelPaint.setColor(0xFF000000);

        _gridPaint = new Paint(Paint.ANTI_ALIAS_FLAG);
        _gridPaint.setColor(0xFFCCCCCC);
    }

    public void setViewSize(float viewSize) {
        _viewSize = viewSize;
    }

    public void drawGrid(boolean drawGrid) {
        _drawGrid = drawGrid;
    }

    @Override
    public void draw(Canvas canvas) {
        float gameWidth = _game.getWidth();
        float gameHeight = _game.getHeight();

        float ps = Math.min((_viewSize - 1) / gameWidth, (_viewSize - 1) / gameHeight);
        Point p = new Point();

        for (p.x = 0; p.x < gameWidth; ++p.x) {
            for (p.y = 0; p.y < gameHeight; ++p.y) {
                if (_game.getCell(p)) {
                    canvas.drawRect(
                            p.x * ps, p.y * ps,
                            (p.x + 1) * ps, (p.y + 1) * ps,
                            _pixelPaint);
                }
            }
        }

        if (_drawGrid) {
            for (int i = 0; i <= gameHeight; ++i)
                canvas.drawLine(0, i * ps, ps * gameWidth, i * ps, _gridPaint);
            for (int i = 0; i <= gameWidth; ++i)
                canvas.drawLine(i * ps, 0, i * ps, ps * gameHeight, _gridPaint);
        }
    }

    @Override
    public void setAlpha(int alpha) {
        _pixelPaint.setAlpha(alpha);
        _gridPaint.setAlpha(alpha);
    }

    @Override
    public void setColorFilter(ColorFilter colorFilter) {
        _pixelPaint.setColorFilter(colorFilter);
        _gridPaint.setColorFilter(colorFilter);
    }

    @Override
    public int getOpacity() {
        return PixelFormat.TRANSLUCENT;
    }
}
