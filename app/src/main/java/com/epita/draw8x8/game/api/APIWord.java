package com.epita.draw8x8.game.api;

import com.epita.draw8x8.game.models.Word;
import com.epita.draw8x8.utils.Consumer;

public class APIWord extends APIModel<Word> {
    public static final String URL_PREFIX = "word";

    public APIWord() {
        super(URL_PREFIX, Word.FACTORY);
    }

    public void getRandom(Consumer<Word> consumer) {
        get(makeUrl(URL_PREFIX, "random"), _factory, consumer);
    }

    public Word save(Word word) {
        return add(word);
    }
}
