package com.epita.draw8x8.game.api;

import android.text.TextUtils;
import android.util.Log;

import com.epita.draw8x8.game.models.Answer;
import com.epita.draw8x8.game.models.Model;
import com.epita.draw8x8.utils.Consumer;

import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.List;

public abstract class APIModel<T extends Model> {
    private String _urlPrefix;
    protected final Model.ModelFactory<T> _factory;

    public APIModel(String urlPrefix, Model.ModelFactory<T> factory) {
        _urlPrefix = urlPrefix;
        _factory = factory;
    }

    protected static URL makeUrl(Object... route) {
        try {
            return new URL("http://" + GameAPI.getIp() + ":" + GameAPI.getPort() + "/" + TextUtils.join("/", route));
        } catch (MalformedURLException e) {
            Log.e("EXCEPTION", Arrays.toString(e.getStackTrace()));
        }

        return null;
    }

    protected static <T extends Model> T get(URL url, Model.ModelFactory<T> factory) {
        return ObjectRequest.get(url, factory);
    }

    protected static <T extends Model> void get(URL url, Model.ModelFactory<T> factory, Consumer<T> consumer) {
        ObjectRequestAsync.getModel(url, factory, consumer);
    }

    protected static <T extends Model> List<T> getArray(URL url, Model.ModelFactory<T> factory) {
        return ObjectRequest.getArray(url, factory);
    }

    protected static <T extends Model> void getArray(URL url, Model.ModelFactory<T> factory, Consumer<List<T>> consumer) {
        ObjectRequestAsync.getModelArray(url, factory, consumer);
    }

    protected static <T extends Model> T post(URL url, Model.ModelFactory<T> factory, Model data) {
        return ObjectRequest.post(url, data, factory);
    }

    protected static <T extends Model> T post(URL url, Model.ModelFactory<T> factory, List<Model> data) {
        return ObjectRequest.post(url, data, factory);
    }

    protected static <T extends Model> void post(URL url, Model.ModelFactory<T> factory, Model data, Consumer<T> consumer) {
        ObjectRequestAsync.postModel(url, data, factory, consumer);
    }

    protected static <T extends Model> void post(URL url, Model.ModelFactory<T> factory, List<Model> data, Consumer<T> consumer) {
        ObjectRequestAsync.postModel(url, data, factory, consumer);
    }

    protected static <T extends Model> List<T> postArray(URL url, Model.ModelFactory<T> factory, Model data) {
        return ObjectRequest.postArray(url, data, factory);
    }

    protected static <T extends Model> List<T> postArray(URL url, Model.ModelFactory<T> factory, List<Model> data) {
        return ObjectRequest.postArray(url, data, factory);
    }

    protected static <T extends Model> void postArray(URL url, Model.ModelFactory<T> factory, Model data, Consumer<List<T>> consumer) {
        ObjectRequestAsync.postModelArray(url, data, factory, consumer);
    }

    protected static <T extends Model> void postArray(URL url, Model.ModelFactory<T> factory, List<Model> data, Consumer<List<T>> consumer) {
        ObjectRequestAsync.postModelArray(url, data, factory, consumer);
    }

    public T getById(String id) {
        return get(makeUrl(_urlPrefix, id), _factory);
    }

    public void getById(String id, Consumer<T> consumer) {
        get(makeUrl(_urlPrefix, id), _factory, consumer);
    }

    public T add(T data) {
        return post(makeUrl(_urlPrefix), _factory, data);
    }

    public void add(T data, Consumer<T> consumer) {
        post(makeUrl(_urlPrefix), _factory, data, consumer);
    }
}
