package com.epita.draw8x8.game.api;

import android.util.Log;

import com.epita.draw8x8.game.models.Model;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.lang.reflect.Constructor;
import java.net.ConnectException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public abstract class ObjectRequest {

    private String performRequest(InputStream inputStream) {
        BufferedReader in = new BufferedReader(new InputStreamReader(inputStream));

        try {
            StringBuilder builder = new StringBuilder();

            String line;
            while (null != (line = in.readLine()))
                builder.append(line);

            return builder.toString();
        } catch (IOException ignored) {}

        return null;
    }

    public <T extends Model> T request(URL url, Model.ModelFactory<T> factory) {
        HttpURLConnection httpURLConnection = null;
        String response = null;

        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            before(httpURLConnection);
            response = performRequest(httpURLConnection.getInputStream());
            return newModelInstance(response, factory);
        } catch (IOException e) {
            if (e.getCause() instanceof ConnectException)
                Log.e("SERVER", "Server is down");
            else
                Log.e("EXCEPTION", e.getMessage());
        } finally {
            if (httpURLConnection != null) {
                try {
                    after(httpURLConnection, response);
                } catch (IOException e) {
                    Log.e("EXCEPTION", e.getMessage());
                }

                httpURLConnection.disconnect();
            }
        }

        return null;
    }

    public  <T extends Model> List<T> requestArray(URL url, Model.ModelFactory<T> factory) {
        HttpURLConnection httpURLConnection = null;

        try {
            httpURLConnection = (HttpURLConnection) url.openConnection();
            before(httpURLConnection);
            String response = performRequest(httpURLConnection.getInputStream());
            after(httpURLConnection, response);
            return newModelArrayInstance(response, factory);
        } catch (IOException e) {
            Log.e("EXCEPTION", e.getMessage());
        } finally {
            if (httpURLConnection != null)
                httpURLConnection.disconnect();
        }

        return null;
    }

    protected abstract void before(HttpURLConnection httpURLConnection) throws IOException;

    protected abstract void after(HttpURLConnection httpURLConnection, String response) throws IOException;

    private static abstract class DefaultObjectRequest extends ObjectRequest {
        protected static void logRequest(HttpURLConnection httpURLConnection, String data) {
            Log.d("REQUEST", String.format("%s %s",
                    httpURLConnection.getRequestMethod(),
                    httpURLConnection.getURL().getPath()));
            if (null != data)
                Log.d("REQUEST", data);
        }

        protected static void logResponse(HttpURLConnection httpURLConnection, String data) {
            try {
                Log.d("RESPONSE", String.format("%s (%s)",
                        httpURLConnection.getResponseCode(),
                        httpURLConnection.getResponseMessage()));
                if (null != data)
                    Log.d("RESPONSE", data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private static class GetObjectRequest extends DefaultObjectRequest {
        @Override
        protected void before(HttpURLConnection httpURLConnection) throws IOException {
            logRequest(httpURLConnection, null);
        }

        @Override
        protected void after(HttpURLConnection httpURLConnection, String data) throws IOException {
            logResponse(httpURLConnection, data);
        }
    }

    private static class PostObjectRequest extends DefaultObjectRequest {
        private String _data;

        private PostObjectRequest(String data) {
            _data = data;
        }

        public static <T extends Model> PostObjectRequest create(T object) {
            JSONObject data = object.toJSON();
            if (null == data)
                return null;

            return new PostObjectRequest(data.toString());
        }

        public static <T extends Model> PostObjectRequest create(List<T> data) {
            JSONArray json = new JSONArray();

            for (Model m : data) {
                JSONObject o = m.toJSON();
                if (null == o)
                    return null;

                json.put(o);
            }

            return new PostObjectRequest(data.toString());
        }

        @Override
        protected void before(HttpURLConnection httpURLConnection) throws IOException {
            httpURLConnection.setRequestMethod("POST");
            httpURLConnection.setRequestProperty("Content-Type", "application/json");
            httpURLConnection.setDoOutput(true);

            BufferedWriter out = new BufferedWriter(new OutputStreamWriter(httpURLConnection.getOutputStream()));
            out.write(_data);
            out.flush();

            logRequest(httpURLConnection, _data);
        }

        @Override
        protected void after(HttpURLConnection httpURLConnection, String data) throws IOException {
            logResponse(httpURLConnection, data);
        }
    }

    public static <T extends Model> T get(URL url, Model.ModelFactory<T> factory) {
        return new GetObjectRequest().request(url, factory);
    }

    public static <T extends Model> List<T> getArray(URL url, Model.ModelFactory<T> factory) {
        return new GetObjectRequest().requestArray(url, factory);
    }

    public static <T extends Model> T post(URL url, Model data, Model.ModelFactory<T> factory) {
        PostObjectRequest por = PostObjectRequest.create(data);
        if (null == por)
            return null;
        return por.request(url, factory);
    }

    public static <T extends Model> T post(URL url, List<Model> data, Model.ModelFactory<T> factory) {
        PostObjectRequest por = PostObjectRequest.create(data);
        if (null == por)
            return null;
        return por.request(url, factory);
    }

    public static <T extends Model> List<T> postArray(URL url, Model data, Model.ModelFactory<T> factory) {
        PostObjectRequest por = PostObjectRequest.create(data);
        if (null == por)
            return null;
        return por.requestArray(url, factory);
    }

    public static <T extends Model> List<T> postArray(URL url, List<Model> data, Model.ModelFactory<T> factory) {
        PostObjectRequest por = PostObjectRequest.create(data);
        if (null == por)
            return null;
        return por.requestArray(url, factory);
    }

    private static <T extends Model> T newModelInstance(String json, Model.ModelFactory<T> factory) {
        if (null == json || 0 == json.length())
            return null;

        try {
            return factory.fromJSON(new JSONObject(json));
        } catch (JSONException e) {
            Log.e("JSON", "Invalid JSON from server: " + e.getMessage() + ": " + json);
        }

        return null;
    }

    private static <T extends Model> List<T> newModelArrayInstance(String json, Model.ModelFactory<T> factory) {
        if (null == json || 0 == json.length())
            return null;

        try {
            List<T> list = new ArrayList<>();
            JSONArray array = new JSONArray(json);

            for (int i = 0; i < array.length(); ++i) {
                list.add(factory.fromJSON(array.getJSONObject(i)));
            }

            return list;
        } catch (JSONException e) {
            Log.e("JSON", "Invalid JSON from server: " + json);
        }

        return null;
    }
}
