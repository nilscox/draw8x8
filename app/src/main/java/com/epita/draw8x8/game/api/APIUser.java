package com.epita.draw8x8.game.api;

import com.epita.draw8x8.game.models.Game;
import com.epita.draw8x8.game.models.Model;
import com.epita.draw8x8.game.models.User;
import com.epita.draw8x8.utils.Consumer;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

public class APIUser extends APIModel<User> {
    public static final String URL_PREFIX = "user";

    public APIUser() {
        super(URL_PREFIX, User.FACTORY);
    }

    public User find(String username) {
        return get(makeUrl(URL_PREFIX, username), _factory);
    }

    public void find(String username, Consumer<User> consumer) {
        get(makeUrl(URL_PREFIX, username), _factory, consumer);
    }

    public User login(User data) {
        return post(makeUrl(URL_PREFIX, "login"), _factory, data);
    }

    public void login(User data, Consumer<User> consumer) {
        post(makeUrl(URL_PREFIX, "login"), _factory, data, consumer);
    }

    public User logout() {
        return get(makeUrl(URL_PREFIX, "logout"), null);
    }

    public void logout(Consumer<User> consumer) {
        get(makeUrl(URL_PREFIX, "logout"), null, consumer);
    }
}
