package com.epita.draw8x8.game.models;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

public abstract class Model implements Serializable {
    public interface ModelFactory<T extends Model> {
        T fromJSON(JSONObject json) throws JSONException;
    }

    protected String _id;
    protected boolean _hasChanged = true;

    public String getId() {
        return _id;
    }

    public boolean hasChanged() {
        return _hasChanged;
    }

    public void setChanged(boolean changed) {
        _hasChanged = changed;
    }

    protected abstract Map<String, Object> getFields();

    @Override
    public boolean equals(Object o) {
        return !(null == o || getClass() != o.getClass()) &&
                _id != null &&
                _id.equals(((Model) o)._id);
    }

    private static JSONArray serializeArray(List<Object> array) throws JSONException {
        JSONArray json = new JSONArray();

        for (Object value : array)
            json.put(serializeObject(value));

        return json;
    }

    private static JSONObject serializeMap(Map<String, Object> map) throws JSONException {
        JSONObject json = new JSONObject();

        for (Map.Entry<String, Object> field : map.entrySet()) {
            if (null != field.getValue()) {
                json.put(field.getKey(), serializeObject(field.getValue()));
            }
        }

        return json;
    }

    private static JSONObject serializeModel(Model model) throws JSONException {
        Map<String, Object> fields = model.getFields();
        if (null == fields)
            return null;
        return serializeMap(fields);
    }

    private static Object serializeObject(Object object) throws JSONException {
        if (object instanceof List)
            return serializeArray((List) object);
        if (object instanceof Map)
            return serializeMap((Map) object);
        else if (object instanceof Model)
            return serializeModel((Model) object);
        else if ((object instanceof String) || (object instanceof Integer))
            return object;
        else
            throw new IllegalStateException();
    }

    public final JSONObject toJSON() {
        try {
            return serializeModel(this);
        } catch (JSONException e) {
            Log.w("JSON", getClass().getName() + "::toJSON failed");
        }

        return null;
    }
}
