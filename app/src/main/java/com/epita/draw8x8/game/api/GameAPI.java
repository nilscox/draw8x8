package com.epita.draw8x8.game.api;

import java.net.CookieHandler;
import java.net.CookieManager;

public class GameAPI {
    private static String _ip;
    private static String _port;

    private static CookieManager cookieManager = new CookieManager();
    static { CookieHandler.setDefault(cookieManager); }

    public static String getIp() {
        return _ip;
    }

    public static void setIp(String ip) {
        _ip = ip;
    }

    public static String getPort() {
        return _port;
    }

    public static void setPort(String port) {
        _port = port;
    }

    public static final APIGame game = new APIGame();
    public static final APIUser user = new APIUser();
    public static final APIWord word = new APIWord();
    public static final APIAnswer answer = new APIAnswer();
}
