package com.epita.draw8x8.game.api;

import com.epita.draw8x8.game.models.Answer;
import com.epita.draw8x8.utils.Consumer;

import java.util.List;

public class APIAnswer extends APIModel<Answer> {
    public static final String URL_PREFIX = "answer";

    public APIAnswer() {
        super(URL_PREFIX, Answer.FACTORY);
    }

    public void getFromUser(String id, Consumer<List<Answer>> consumer) {
        getArray(makeUrl(URL_PREFIX, "user", id), Answer.FACTORY, consumer);
    }
}
