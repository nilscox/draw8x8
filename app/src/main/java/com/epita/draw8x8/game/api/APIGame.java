package com.epita.draw8x8.game.api;

import com.epita.draw8x8.game.models.Game;
import com.epita.draw8x8.utils.Consumer;

import java.util.List;

public class APIGame extends APIModel<Game> {
    public static final String URL_PREFIX = "game";

    public APIGame() {
        super(URL_PREFIX, Game.FACTORY);
    }

    public Game getRandom() {
        return get(makeUrl(URL_PREFIX, "random"), _factory);
    }

    public void getRandom(Consumer<Game> consumer) {
        get(makeUrl(URL_PREFIX, "random"), _factory, consumer);
    }

    public void getFromUser(String id, Consumer<List<Game>> consumer) {
        getArray(makeUrl(URL_PREFIX, "user", id), Game.FACTORY, consumer);
    }
}
