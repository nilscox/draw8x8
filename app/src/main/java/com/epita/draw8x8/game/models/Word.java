package com.epita.draw8x8.game.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class Word extends Model {
    public static final WordFactory FACTORY = new WordFactory();

    private String _word;

    public Word(String word) {
        _word = word;
    }

    static class WordFactory implements ModelFactory<Word> {
        @Override
        public Word fromJSON(JSONObject json) throws JSONException {
            Word word = new Word(json.getString("word"));
            word._id = json.getString("id");
            word.setChanged(false);
            return word;
        }
    }

    @Override
    protected Map<String, Object> getFields() {
        Map<String, Object> map = new HashMap<>();

        map.put("id", _id);
        map.put("word", _word);

        return map;
    }

    public String getWord() {
        return _word;
    }

    @Override
    public String toString() {
        return getWord();
    }
}
