package com.epita.draw8x8.game.api;

import android.os.AsyncTask;

import com.epita.draw8x8.game.models.Model;
import com.epita.draw8x8.utils.Consumer;

import java.net.URL;
import java.util.List;

public abstract class ObjectRequestAsync {
    private static abstract class ObjectRequestSingle<T extends Model> extends AsyncTask<URL, Void, T> {
        private Consumer<T> _consumer;
        public ObjectRequestSingle(Consumer<T> consumer) {
            _consumer = consumer;
        }

        @Override
        protected void onPostExecute(T result) {
            _consumer.accept(result);
        }
    }

    private static abstract class ObjectRequestArray<T extends Model> extends AsyncTask<URL, Void, List<T>> {
        private Consumer<List<T>> _consumer;
        public ObjectRequestArray(Consumer<List<T>> consumer) {
            _consumer = consumer;
        }

        @Override
        protected void onPostExecute(List<T> result) {
            _consumer.accept(result);
        }
    }

    public static <T extends Model> void getModel(URL url, final Model.ModelFactory<T> factory, Consumer<T> consumer) {
        new ObjectRequestSingle<T>(consumer) {
            @Override
            protected T doInBackground(URL... urls) {
                return ObjectRequest.get(urls[0], factory);
            }
        }.execute(url);
    }

    public static <T extends Model> void getModelArray(URL url, final Model.ModelFactory<T> factory, Consumer<List<T>> consumer) {
        new ObjectRequestArray<T>(consumer) {
            @Override
            protected List<T> doInBackground(URL... urls) {
                return ObjectRequest.getArray(urls[0], factory);
            }
        }.execute(url);
    }

    public static <T extends Model> void postModel(URL url, final Model data, final Model.ModelFactory<T> factory, Consumer<T> consumer) {
        new ObjectRequestSingle<T>(consumer) {
            @Override
            protected T doInBackground(URL... urls) {
                return ObjectRequest.post(urls[0], data, factory);
            }
        }.execute(url);
    }

    public static <T extends Model> void postModel(URL url, final List<Model> data, final Model.ModelFactory<T> factory, Consumer<T> consumer) {
        new ObjectRequestSingle<T>(consumer) {
            @Override
            protected T doInBackground(URL... urls) {
                return ObjectRequest.post(urls[0], data, factory);
            }
        }.execute(url);
    }

    public static <T extends Model> void postModelArray(URL url, final Model data, final Model.ModelFactory<T> factory, Consumer<List<T>> consumer) {
        new ObjectRequestArray<T>(consumer) {
            @Override
            protected List<T> doInBackground(URL... urls) {
                return ObjectRequest.postArray(urls[0], data, factory);
            }
        }.execute(url);
    }

    public static <T extends Model> void postModelArray(URL url, final List<Model> data, final Model.ModelFactory<T> factory, Consumer<List<T>> consumer) {
        new ObjectRequestArray<T>(consumer) {
            @Override
            protected List<T> doInBackground(URL... urls) {
                return ObjectRequest.postArray(urls[0], data, factory);
            }
        }.execute(url);
    }
}
