package com.epita.draw8x8.game.models;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class User extends Model {
    public static final UserFactory FACTORY = new UserFactory();

    private String _username;

    public User(String username) {
        _username = username;
    }

    static class UserFactory implements ModelFactory<User> {
        @Override
        public User fromJSON(JSONObject json) throws JSONException {
            User user = new User(json.getString("username"));
            user._id = json.getString("id");
            user.setChanged(false);
            return user;
        }
    }

    @Override
    public Map<String, Object> getFields() {
        Map<String, Object> map = new HashMap<>();

        map.put("id", _id);
        map.put("username", _username);

        return map;
    }

    public String getUsername() {
        return _username;
    }

    @Override
    public String toString() {
        return getUsername();
    }
}
