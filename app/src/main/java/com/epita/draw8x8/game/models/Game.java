package com.epita.draw8x8.game.models;

import android.graphics.Point;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Game extends Model {
    public static final GameFactory FACTORY = new GameFactory();

    private User _author;
    private Word _word;

    private int _width;
    private int _height;
    private boolean[][] _cells;

    public Game(User user, Word word, int width, int height) {
        if (null == user || null == user.getId() ||
            null == word || null == word.getId() ||
            width <= 0 || height <= 0)
            throw new IllegalArgumentException();

        _author = user;
        _word = word;
        _width = width;
        _height = height;

        _cells = new boolean[_width][_height];
        for (int i = 0; i < _width; ++i)
            for (int j = 0; j < _height; ++j)
                _cells[i][j] = false;
    }

    static class GameFactory implements ModelFactory<Game> {
        public Game fromJSON(JSONObject json) throws JSONException {
            String id = json.getString("id");
            User user = User.FACTORY.fromJSON(json.getJSONObject("author"));
            Word word = Word.FACTORY.fromJSON(json.getJSONObject("word"));

            int width = json.getInt("width");
            int height = json.getInt("height");

            JSONArray lines = json.getJSONArray("game");
            if (lines.length() != width)
                throw new IllegalStateException();

            Game game = new Game(user, word, width, height);
            game._id = id;

            Point p = new Point();
            for (p.x = 0; p.x < width; ++p.x) {
                JSONArray line = lines.getJSONArray(p.x);
                if (line.length() != height)
                    throw new IllegalStateException();

                for (p.y = 0; p.y < height; ++p.y)
                    game.setCell(p, line.getInt(p.y) != 0);
            }

            game.setChanged(false);
            return game;
        }
    }

    @Override
    public Map<String, Object> getFields() {
        Map<String, Object> map = new HashMap<>();

        map.put("width", _width);
        map.put("height", _height);
        map.put("authorId", _author.getId());
        map.put("wordId", _word.getId());

        List<List<Integer>> lines = new ArrayList<>();
        for (int i = 0; i < _width; ++i) {
            List<Integer> line = new ArrayList<>();
            for (int j = 0; j < _height; ++j)
                line.add(_cells[i][j] ? 1 : 0);
            lines.add(line);
        }

        map.put("game", lines);

        return map;
    }

    public boolean getCell(Point p) {
        return _cells[p.x][p.y];
    }

    public void setCell(Point p, boolean value) {
        if (p.x < 0 || p.x >= _width || p.y < 0 || p.y >= _height) {
            Log.d("TEST", p.toString());
            return;
        }
        if (_cells[p.x][p.y] != value) {
            _cells[p.x][p.y] = value;
            setChanged(true);
        }
    }

    public int getWidth() {
        return _width;
    }

    public int getHeight() {
        return _height;
    }

    public Word getWord() {
        return _word;
    }

    public User getUser() {
        return _author;
    }

    public boolean isAnswerCorrect(Answer answer) {
        return answer.getWord().equals(_word);
    }
}
