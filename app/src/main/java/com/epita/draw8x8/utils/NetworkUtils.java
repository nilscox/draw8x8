package com.epita.draw8x8.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.AsyncTask;

import com.epita.draw8x8.game.api.GameAPI;

import java.io.IOException;
import java.net.ConnectException;
import java.net.URL;

public class NetworkUtils {
    public static void isServerUp(final Consumer<Boolean> consumer) {
        new AsyncTask<Void, Void, Boolean>() {
            @Override
            protected Boolean doInBackground(Void... params) {
                try {
                    URL url = new URL("http://" + GameAPI.getIp() + ":" + GameAPI.getPort());
                    url.openConnection().getInputStream();
                } catch (IOException e) {
                    return !(e instanceof ConnectException);
                }
                return true;
            }

            @Override
            protected void onPostExecute(Boolean value) {
                consumer.accept(value);
            }
        }.execute();
    }

    public static boolean isConnected(Context context) {
        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetwork = cm.getActiveNetworkInfo();

        if (null != activeNetwork) {
            int type = activeNetwork.getType();
            return type == ConnectivityManager.TYPE_WIFI || type == ConnectivityManager.TYPE_MOBILE;
        }

        return false;
    }
}
