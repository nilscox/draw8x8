package com.epita.draw8x8.utils;

public interface Consumer<T> {
    void accept(T t);
}
